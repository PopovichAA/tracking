import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

const SERVER_URL = "http://163.172.172.204";

Meteor.methods({
	/**
	 * Получение информации о заказе
	 * @params {string} trackId - идентификатор заказа
	 */
  	 getTrackingInfo( { trackId = "" } ) {
 		return HTTP.get( `${SERVER_URL}/data/TrackPackage${trackId}.html` );
	}
});

Meteor.startup(() => {
   
});
 