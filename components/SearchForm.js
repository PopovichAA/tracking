import React, { Component } from "react";

/**
 * Форма для поиска 
 */
class SearchForm extends Component {
	/**
	 * Конструктор
	 * @params {object} props - свойства
	 */
	constructor( props ) {
		super( props );

		this.changeHandler = this.changeHandler.bind( this );
		this.submitHandler = this.submitHandler.bind( this );
	}

	/**
	 * Обработка изменения текста поиска
	 * @params {object} event - событие
	 */
	changeHandler( event ) {
		this.props.textChangeHandler( event.target.value );
	}

	/**
	 * Обработка поиска
	 * @params {object} event - событие
	 */
	submitHandler( event ) {
		event.preventDefault();
		this.props.searchHandler();
	}

	/**
	 * Отрисовка
	 */ 
	render() {
		let props = this.props;
    	return (
			<form onSubmit={this.submitHandler}>
		        <label>
		          {props.children}
		          <input type="text" value={props.searchText} onChange={this.changeHandler} placeholder=""/>
		        </label>
		        <input type="submit" value="Search" />
	      	</form>
      	);
	}
}

export default SearchForm;