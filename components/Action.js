import React from "react";
/**
 * Информация о действии
 * @params {object} props - свойства
 */
function Action( props ) {
	let actionInfo = [];

	let action = props.action;

	// показываем только определенные действия
	let propertiesForShow = ["date", "time", "action", "place"];
	propertiesForShow.forEach( ( property ) => {
		if( props[property + "Show"] && action[property] ) {
			actionInfo.push( action[property] );
		}
	} );

	return (
		<div>{actionInfo.join( " / " )}</div>
	);
}

export default Action;