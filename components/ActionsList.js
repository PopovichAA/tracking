import React from "react";
import Action from "./Action";

/**
 * Спиоск действий
 * @params {object} props - свойства
 */
function ActionsList( { actions = [] } ) {
	let actionsCount = actions.length;

	let actionElements = actions.map( ( action, index ) => {
		// определяем какие свойства показывать
		let visibilityOptions = {  dateShow : true, timeShow : true, placeShow : true, actionShow : false };
		switch( index ) {
			case 0:
				visibilityOptions.actionShow = true;
				break;
			case (actionsCount - 1):
				visibilityOptions.actionShow = true;
				visibilityOptions.placeShow = false;
				break;
			default:
				break;
		}

		return <Action {...visibilityOptions} key={index} action={action}/>;
	} );
	return (<div>{actionElements}</div>);
}

export default ActionsList;