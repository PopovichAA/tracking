import React, { Component } from "react";
import { Meteor } from 'meteor/meteor';
import SearchForm from './SearchForm';
import ActionsList from './ActionsList';

/**
 * Основной компонент приложения 
 */
class App extends Component {
	/**
	 * Конструктор
	 * @params {object} props - свойства
	 */
	constructor( props ) {
		super( props );

		this.state = {
			searchText : "",
			actions    : []
		};

		this.searchHandler = this.searchHandler.bind( this );
		this.textChangeHandler = this.textChangeHandler.bind( this );		
	}

	/**
	 * Получение текста блока
	 * @params {node} box - блок
	 * @returns {string}
	 */
	getBoxText( box ) {
		let result = "";
        if( box ) {
            result = box.textContent.trim().replace( /[\s\t\n]{1,}/g, " " );
        }
        return result;
	}

	/**
	 * Обработка полученного ответа 
	 * @params {string} textContent - текст ответа
	 * @returns {array}
	 */
	parse( textContent = "" ) {		
	    // массив с информацией
	    let actions = [];

	    // получаем блоки с информацией
	    let parser = new DOMParser();
		let doc = parser.parseFromString( textContent, "text/html" );	    
	    if( doc ) {
	    	let historyDiv = doc.querySelector( "div[data-a-expander-name=event-history-list" );
	    	if( historyDiv ) {
	    		let boxes = historyDiv.querySelectorAll( ".a-box" );

			    let date = "";
			    for( let i = 0, toi = boxes.length; i < toi; i++ ) {
			        let box = boxes[i];
			        let text = this.getBoxText( box );
			        let classes = box.getAttribute( "class" );

			        // если дата
			        if( classes.indexOf( "a-color-alternate-background" ) >= 0 ) {
			            date = text.replace( "Latest update: ", "" );
			        }
			        // считаем что это блок с информацией
			        else {
			            // время
			            let timeBox = box.querySelector( ".ship-track-grid-fixed-column" );
			            let time = this.getBoxText( timeBox );

			            // место
			            let placeBox = box.querySelector( ".a-color-secondary" );
			            let place = this.getBoxText( placeBox );

			            // действие
			            let actionBox = box.querySelector( "span" );
			            let action = this.getBoxText( actionBox );

			            actions.push( { date, time, place, action } );
			        }
			    }
	    	}		    
	    }
	    
	    return actions;
	}

	/**
	 * Обработка поиска
	 */
	searchHandler() {
		let trackId = this.state.searchText;
		if( trackId.length > 0 ) {
			Meteor.call( "getTrackingInfo", { trackId }, ( error, res ) =>  {
				let actions = [];
				if( !error ) {
					actions = this.parse( res.content );
				} else {
					console.error( `Error in getTrackingInfo. Error: ${error}` );
				}
				this.setState( { actions : actions } );
			} );
		} else {
			this.setState( { actions : [] } );
		}		
	}

	/**
	 * Обработка изменения текста поиска
	 * @params {string} value - текст поиска
	 */
	textChangeHandler( value = "" ) {
		this.setState( { searchText : value } );
	}

	/**
	 * Отрисовка
	 */ 
	render() {
		return (
			<div>
				<SearchForm searchHandler={this.searchHandler} searchText={this.state.searchText} textChangeHandler={this.textChangeHandler}>
					Carrier: UBH Expeditors, Tracking #:
				</SearchForm>
				<b>INFORMATION</b>

				<ActionsList actions={this.state.actions}/>
			</div>
		);	
	}
}

export default App;